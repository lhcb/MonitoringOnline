import os
import atexit
from Hlt2Monitoring.Manager import Manager

utgid = 'TEST_DISKMONITOR_00'

cmd = """
import socket
from Monitoring import DiskMonitor
DiskMonitor.run(SavePath='/tmp/histograms',
                RegistrarConnection='tcp://%s:31360' % socket.gethostname(),
                DimDNSNode='mona08')
"""

os.environ['DIM_DNS_NODE'] = 'mona08'
env = {'LC_ALL': 'C', 'UTGID': utgid, 'PARTITION': 'LHCb',
       'PARTITION_NAME': 'LHCb'}

command = 'python -c "%s"' % cmd
manager = Manager(utgid, command, env)

atexit.register(manager.terminate)
