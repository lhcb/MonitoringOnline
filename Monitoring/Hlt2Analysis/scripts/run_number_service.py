from __future__ import print_function
import os
import pydim
from time import sleep
from Monitoring.TestUtilities import runs_from_savesets


class RunNumberService(object):

    def __init__(self, runNumber):
        self.__runNumber = runNumber
        utgid = "RUNNUMBERTEST"
        try:
            self.__info_svc = pydim.dis_add_service(
                "LHCbStatus/RunNumber", "i", self.__callback, 0)
        except:
            print(os.environ['DIM_DNS_NODE'])
            print(utgid)
            raise
        pydim.dis_start_serving(utgid)

        pydim.dis_update_service(self.__info_svc)

    def run_number(self):
        return self.__runNumber

    def set_run_number(self, rn):
        self.__runNumber = rn
        pydim.dis_update_service(self.__info_svc)

    def __callback(self, *args):
        return (self.__runNumber,)


saveset_dir = "/hist/Savesets/ByRun/Moore2Saver/210000/210000"
runs = runs_from_savesets(saveset_dir)
run = runs[-1] + 1
rns = RunNumberService(run)

while True:
    sleep(36)
    run += 1
    print(run)
    rns.set_run_number(run)
