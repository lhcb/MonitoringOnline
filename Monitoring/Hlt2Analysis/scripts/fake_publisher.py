#!/usr/bin/env python
from __future__ import print_function
import os
import re
import socket
from Monitoring.TestUtilities import Publisher
from GaudiPython.Bindings import AppMgr, InterfaceCast, gbl

run_expr = re.compile(r"Moore2Saver-run(\d+)\.root")

gaudi = AppMgr()
zmqSvc = gaudi.createSvc("ZeroMQSvc")
zmqSvc = InterfaceCast(gbl.IZeroMQSvc)(zmqSvc)
gaudi.initialize()
gaudi.start()

context = zmqSvc.context()
reg_con = "tcp://%s:31360" % socket.gethostname()

files = []
saveset_dir = "/hist/Savesets/ByRun/Moore2Saver/210000/210000"
for d, _, fs in os.walk(saveset_dir):
    for f in fs:
        m = run_expr.match(f)
        if m:
            run = int(m.group(1))
            files.append(os.path.join(d, f))
files = sorted(files)

print("Start test publisher")
test = Publisher("Moore2Saver", context, reg_con, files, 10, True)
test.start()
