#!/bin/bash
# =========================================================================
#
#  Script to start the run completion monitor
#
#  Author   R. Aaij
#  Version: 1.0
#  Date:    15/06/2018
#
# =========================================================================
#
export CMTCONFIG=x86_64-centos7-gcc62-dbg

# environment
. /group/hlt/monitoring/MONITORINGONLINE/MONITORINGONLINE_RELEASE/setup.$CMTCONFIG.vars

export UTGID
export LOGFIFO=/var/run/fmc/logAnalysisLogger.fifo
export PARTITION="LHCb"
export PARTITION_NAME="LHCb"
export DIM_DNS_NODE=mona08
export PYTHONPATH=/group/online/dataflow/options/${PARTITION}/RECONSTRUCTION:$PYTHONPATH

export PYTHONHOME=`python -c 'import sys; print sys.prefix'`

read COMMAND <<EOF
from Hlt2Analysis import DiskMonitor;\
DiskMonitor.run(DimDNSNode='mona08',\
                  OutputLevel=3)
EOF

echo "[DEBUG] RunCompletion Monitor on ${HOSTNAME}"
exec -a ${UTGID} python -c "${COMMAND}"
