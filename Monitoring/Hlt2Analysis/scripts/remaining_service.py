from __future__ import print_function
import os
import re
import pydim
from time import sleep
from Monitoring.TestUtilities import runs_from_savesets


class RemainingService(object):

    def __init__(self):
        self.__remaining = ""
        utgid = "REMAININGTEST"
        try:
            self.__info_svc = pydim.dis_add_service(
                "/HLT/HLT1/Runs", "C", self.__callback, 0)
        except Exception as e:
            print(str(e))
            print(os.environ['DIM_DNS_NODE'])
            print(utgid)
            raise e
        pydim.dis_start_serving(utgid)

        pydim.dis_update_service(self.__info_svc)

    def run_number(self):
        return self.__remaining

    def set_remaining(self, remaining):
        self.__remaining = "|".join("{}/{}".format(r, f) for r, f in remaining)
        pydim.dis_update_service(self.__info_svc)

    def __callback(self, *args):
        return (self.__remaining,)


remaining_svc = RemainingService()

n_file_expr = re.compile("RunNFiles\s*=\s*(\d+)")
path_pat = "/group/online/hlt/conditions/LHCb/NoYear/{0}/{1}/HLT2Params.py"

saveset_dir = "/hist/Savesets/ByRun/Moore2Saver/210000/210000"
runs = runs_from_savesets(saveset_dir)


def check_produced(run):
    f = path_pat.format(run / 1000, run)
    msg = "Checking %d" % run
    prod = None
    if os.path.exists(f):
        with open(path_pat.format(run / 1000, run)) as f:
            for line in f:
                m = n_file_expr.match(line)
                if m:
                    prod = int(m.group(1))
    if prod is None:
        msg += "; generated python file does not exist."
    else:
        msg += ("; %10d files produced." % prod)
    print(msg)
    return prod


produced = {r: p for r, p in [(r, check_produced(r)) for r in runs] if p}
remaining = [(r, produced[r]) for r in sorted(produced.keys())]

while remaining:
    sleep(10)
    remaining_svc.set_remaining(remaining)
    remaining = [(r, p - max(produced[r] / 50, 2)) for r, p in remaining]
    remaining = [(r, p) for r, p in remaining if p > 0]
