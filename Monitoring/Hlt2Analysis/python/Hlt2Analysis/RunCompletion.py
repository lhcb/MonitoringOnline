import re
import os
import socket
from itertools import islice
from Monitoring.DimMonitor import DimForwarder
from Monitoring.decorators import zmq
from Configurables import MonitoringJob
from multiprocessing import Process, Pipe
from ROOT import TH1D
from Monitoring.Communicator import (Communicator, State)
from Monitoring.Analyser import Analyser
from RunDBAPI import rundbapi
from RunDBAPI.rundbapi import RequestError

n_file_re = re.compile("RunNFiles\s*=\s*(\d+)")
path_pat = "/group/online/hlt/conditions/LHCb/NoYear/{0}/{1}/HLT2Params.py"


class Completion(Analyser):
    def __init__(self, name, forward_con, **kwargs):
        super(Completion, self).__init__(name, **kwargs)
        self.__file_info = {}
        self.__fw_con = forward_con
        self.__closed = set()
        self.__max_run = 0
        self.__latest = TH1D("recent_runs", "recent_runs", 51, -0.5, 50.5)
        self.__latest_info = {}
        self.__re_run = re.compile(r"Moore2Saver-run(\d+)\.root")
        # intialize the check counter such that a check will occur on
        # the first info update
        self.__check_counter = 99
        self.__min_run = None

    def check_produced(self, run):
        def __check():
            f = path_pat.format(run / 1000, run)
            msg = "Checking %d" % run
            prod = None
            if os.path.exists(f):
                with open(path_pat.format(run / 1000, run)) as f:
                    for line in f:
                        m = n_file_re.match(line)
                        if m:
                            prod = int(m.group(1))
            if prod is None:
                msg += "; generated python file does not exist."
            else:
                msg += ("; %10d files produced." % prod)
            self.Debug(msg)
            return prod

        if run in self.__file_info:
            if self.__file_info[run][0] is None:
                _, rem = self.__file_info[run]
                self.__file_info[run] = (__check(), rem)
        else:
            self.__file_info[run] = (__check(), None)
        return self.__file_info[run][0]

    def produced(self, run):
        return self.__file_info.get(run, (None, None))[0]

    def remaining(self, run):
        if run in self.__file_info:
            prod, rem = self.__file_info[run]
            if rem is None and run < self.__min_run:
                self.__file_info[run] = (prod, 0.)
        elif run < self.__min_run:
            self.__file_info[run] = (None, 0.)
        return self.__file_info.get(run, (None, None))[1]

    def run_info(self, val):
        info = {}
        if not val.strip():
            return info
        for entry in val.split('|'):
            try:
                k, v = tuple(int(n) for n in entry.split('/'))
                info[k] = v
            except ValueError:
                self.Warning("Failed to parse run info for %s " % entry)
        return info

    def update_info(self, socket):
        self.Verbose('update_info')
        info = self.run_info(socket.recv_string())
        if not info:
            self.Debug("Got no info from DIM")
            return

        self.__check_counter += 1

        self.__min_run = min(info.keys())
        for run in self.__file_info:
            if run < self.__min_run:
                prod, _ = self.__file_info[run]
                self.__file_info[run] = (prod, 0.)

        if not self.__file_info:
            max_run = max(info.keys())
            self.bootstrap(max_run)

        if self.__check_counter == 100:
            self.Debug("Checking python files for number of produced files.")
            for run in set(self.__file_info.keys() + info.keys()):
                self.check_produced(run)
            self.__check_counter = 0

        for run, remaining in info.iteritems():
            produced = self.produced(run)
            # Sometimes DIM reports more files, and is more up to
            # date, so use that as the number of produced files.
            if remaining > produced:
                self.__file_info[run] = (remaining, remaining)
            else:
                self.__file_info[run] = (produced, remaining)
            self.__max_run = max(run, self.__max_run)

    def bootstrap(self, max_run):
        from datetime import datetime

        # Bootstrap from rundb
        self.Debug("Bootstrapping from run DB")

        def _bootstrap(n):
            now = datetime.now()
            rt = 'COLLISION%d' % (now.year - 2000)
            try:
                info = {}
                latest = rundbapi.search_runs(runtype=rt,
                                              LHCState='PHYSICS',
                                              destination='OFFLINE',
                                              veloPosition='CLOSED',
                                              rows=n)
                for ri in latest['runs']:
                    if 'run_state' not in ri:
                        continue
                    state = ri['run_state']
                    run = ri['runid']
                    info[run] = bool(state <= 2)
                    if state <= 2:
                        self.Debug("RunDB: run %d is closed" % run)

                return info
            except (KeyError, RequestError):
                return {}

        n_runs = 100
        n = n_runs

        def n_smaller(runs):
            return sum(map(lambda r: r <= max_run, runs))

        latest = {}
        while n_smaller(latest.keys()) < n_runs:
            latest = _bootstrap(n)
            n += 50

        latest = dict(filter(lambda r: r[0] <= max_run,
                             latest.iteritems()))
        for run, closed in latest.iteritems():
            closed = (closed or run < self.__min_run)
            self.__file_info[run] = (None, 0. if closed else None)

        self.__max_run = max(self.__file_info.keys())

    def start(self):
        dim_socket = self.socket(zmq.SUB)
        dim_socket.setsockopt(zmq.LINGER, 0)
        dim_socket.setsockopt(zmq.SUBSCRIBE, "")
        dim_socket.connect(self.__fw_con)
        self.register_handler(dim_socket, zmq.POLLIN, self.update_info)
        return super(Completion, self).start()

    def analyse(self, app, filename):
        self.Debug('analyse %s %s' % (app, filename))

        run = 0
        try:
            d, f = os.path.split(filename)
            m = self.__re_run.match(f)
            if m:
                run = int(m.group(1))
            else:
                raise IndexError
        except (IndexError, ValueError):
            self.Warning("Failed to extract run number from %s" % filename)
            return

        if len(self.__file_info) == 0:
            self.bootstrap(run)
            self.__check_counter += 1
            if self.__check_counter == 100:
                self.Debug("Checking python files for "
                           "number of produced files.")
                for run in set(self.__file_info.keys()):
                    self.check_produced(run)
                self.__check_counter = 0

        if run > self.__max_run:
            self.Debug("Run too large: %d max is %s" % (run, self.__max_run))
            return

        produced = self.check_produced(run)
        remaining = self.remaining(run)

        if produced is None or remaining is None:
            pi = "None" if produced is None else str(produced)
            ri = "None" if remaining is None else str(remaining)
            self.Debug("No info on produced or remaining: %s %s" % (pi, ri))
            return

        extra = {'completion': [lambda h: h.GetYaxis().SetRangeUser(0, 1)]}

        for name, number in [("produced", produced),
                             ("remaining", remaining),
                             ("completion", float(produced - remaining) / float(produced))]:
            self.Debug("run {:7d} {:<11s} {:9.2f}".format(run, name, number))
            histo = TH1D(name, name, 1, -0.5, 0.5)
            histo.SetBinContent(1, number)
            for fun in extra.get(name, []):
                fun(histo)
            self.publishHistogram(histo, runNumber=run, add=False)

        latest = sorted(self.__file_info.keys())
        for i, (left, right) in enumerate([(-100, -50), (-50, None)]):
            left = len(latest) + left
            right = len(latest) if right is None else len(latest) + right
            name = 'latest_runs_%d' % i
            n = right - left
            latest_histo = TH1D(name, name, n, - 0.5, n - 0.5)
            latest_histo.GetYaxis().SetRangeUser(0., 1.1)
            for bin_num, run in enumerate(islice(latest, left, right)):
                produced = self.produced(run)
                remaining = self.remaining(run)
                comp = 0.
                if remaining == 0.:
                    comp = 1.
                elif produced is not None:
                    comp = float(produced - remaining) / float(produced)
                latest_histo.SetBinContent(bin_num + 1, comp)
                latest_histo.GetXaxis().SetBinLabel(bin_num + 1, str(run))
            self.publishHistogram(latest_histo, add=False)


def run(outputLevel=3, auto=True, forward_from="ecs03", **kwargs):

    forward_pipe, start_pipe = Pipe()
    run_svc = DimForwarder('/HLT/HLT1/Runs',
                           dim_dns_node=forward_from,
                           connection_path="/run/HLT2/RunCompletion",
                           pipe=start_pipe)
    forwarder = Process(target=run_svc)
    forwarder.daemon = True
    # fork must happen here, so start now
    forwarder.start()

    # Start the communicator
    com = Communicator('RUNCOMPLETION')

    # FSM loop
    state = State.NOT_READY
    com.set_status(state)

    gaudi = None
    analyser = None
    control = None

    while True:
        # Handle commands
        command = None
        if not (auto and state in (State.NOT_READY, State.READY)):
            command = com.get_command()

        if ((command and command.startswith('configure') or auto)
                and state == State.NOT_READY):

            mj = MonitoringJob(**kwargs)
            mj.JobName = "RunCompletion"

            import cppyy
            cppyy.gbl.ROOT.EnableThreadSafety()
            from GaudiPython.Bindings import AppMgr
            gaudi = AppMgr()

            reg_con = mj.getProp('RegistrarConnection')
            analyser = Completion('RunCompletionAnalyser',
                                  run_svc.service_connection(),
                                  Applications=['Moore2Saver'],
                                  RegistrarConnection=reg_con,
                                  HistogramDirectory=mj.JobName,
                                  OutputLevel=mj.getProp("OutputLevel"),
                                  RunInMain=False)

            from Monitoring.MonitoringJob import initialize
            initialize(gaudi)

            state = State.READY
        elif ((command and command.startswith('start') or auto)
              and state == State.READY):
            # Really start the forwarder process
            start_pipe.send('start')

            # Start our main job
            from Monitoring.MonitoringJob import start
            monSvc = None
            try:
                _, monSvc = start()
            except RuntimeError, e:
                print e
                state = State.ERROR
                break

            # Wait for forwarder to be ready
            r = forward_pipe.poll(60)
            if r:
                forward_pipe.recv()
            else:
                state = State.ERROR
                break

            # Connect control connection
            zmqSvc = monSvc.zmq()
            control = zmqSvc.socket(zmq.PAIR)
            control.connect(run_svc.control_connection())

            state = State.RUNNING
        elif (command and command.startswith('stop')
              and state in (State.RUNNING, State.READY)):
            if gaudi:
                gaudi.stop()
            state = State.READY
        elif command and command.startswith('reset'):
            if zmqSvc:
                control.send("TERMINATE")
                if forwarder.is_alive():
                    forwarder.join()

            if gaudi:
                gaudi.finalize()
            state = State.NOT_READY
            break
        elif command is not None:
            print('[ERROR]: RunCompletion: bad transition '
                  'from %s to %s' % (state, command))
            state = State.ERROR
            break

        if (command is not None) or (auto and state in (State.NOT_READY,
                                                        State.READY,
                                                        State.RUNNING)):
            # Once we've auto-started, set auto to off to prevent
            # further auto actions, for example after a stop command
            if auto and state == State.RUNNING:
                auto = False
            # Set the status
            com.set_status(state)

    # Set our status one last time
    com.set_status(state)


if __name__ == '__main__':
    run()
