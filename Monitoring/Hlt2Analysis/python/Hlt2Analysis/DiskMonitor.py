from __future__ import print_fuction

import re
import socket
from Monitoring.DimMonitor import DimForwarder
from Monitoring.decorators import zmq
from Hlt2Monitoring.Utilities import node_type
from Configurables import MonitoringJob
from multiprocessing import Process, Pipe
from ROOT import TH1D
from ROOT import kOrange, kGreen, kBlue, kMagenta
from Monitoring.Communicator import (Communicator, State)


def nodes_info(val):
    node_re = re.compile(
        r"hlt(?P<rack>[a-f])(?P<row>[0-9]{2})(?P<node>[0-9]{2})")
    info = {}
    s = val.split('|')
    if not s:
        return info
    for entry in s:
        if not entry:
            continue
        if not entry[0].isalnum():
            break
        host, numbers = entry.split(' ')
        m = node_re.match(host)
        if not m:
            continue
        n = numbers.split('/')
        total, free, nd, gd = float(n[0]), float(n[1]), int(n[2]), int(n[3])
        info[(host, m.group('rack'), int(m.group('row')),
              int(m.group('node')))] = (total, free, nd, gd)
    return info


plots = {0: ("slow", kMagenta - 3),
         1: ("medium", kOrange + 5),
         2: ("fast", kBlue - 3),
         3: ("faster", kGreen - 3)}
histos = {}


def run(outputLevel=3, auto=True, **kwargs):

    forward_pipe, start_pipe = Pipe()
    filling_svc = DimForwarder('FarmStatus/StorageStatus',
                               dim_dns_node="ecs03",
                               connection_path="/tmp/DiskMonitor",
                               pipe=start_pipe)
    forwarder = Process(target=filling_svc)
    forwarder.daemon = True
    # fork must happen here, so start now
    forwarder.start()

    # Start the communicator
    com = Communicator('DISKMONITOR')

    mj = MonitoringJob(**kwargs)
    mj.JobName = "DiskFilling"

    # FSM loop
    state = State.NOT_READY
    com.set_status(state)

    gaudi = None

    print('[DEBUG] DiskMonitor launched')

    poller = zmq.Poller()
    command_pipe = com.pipe()
    poller.register(command_pipe, zmq.POLLIN)

    # socket placeholders
    input_socket = None
    control = None
    zmqSvc = None

    while True:

        if not (auto and state in (State.NOT_READY, State.READY)):
            timeout = -1
        else:
            timeout = 0

        rep = dict(poller.poll(timeout))
        command = None

        # Handle commands
        if command_pipe in rep and rep[command_pipe] == zmq.POLLIN:
            command = com.get_command()
            print('Got command %s' % command)

        if ((command and command.startswith('configure') or auto)
                and state == State.NOT_READY):

            from Monitoring.MonitoringJob import initialize
            initialize()

            for nt, (name, color) in plots.iteritems():
                histo = TH1D(name, name, 101, -0.05, 1.05)
                histo.SetLineColor(color)
                histo.SetLineWidth(2)
                histos[nt] = histo

            state = State.READY
        elif ((command and command.startswith('start') or auto)
              and state == State.READY):
            # Really start the forwarder process
            start_pipe.send('start')

            # Start our main job
            from Monitoring.MonitoringJob import start
            monSvc = None
            try:
                _, monSvc = start()
            except RuntimeError, e:
                print(e)
                state = State.ERROR
                break

            zmqSvc = monSvc.zmq()
            input_socket = zmqSvc.socket(zmq.SUB)
            input_socket.setsockopt(zmq.SUBSCRIBE, "")
            input_socket.setsockopt(zmq.LINGER, 0)
            input_socket.connect(filling_svc.service_connection())
            poller.register(input_socket, zmq.POLLIN)

            # Wait for forwarder to be ready
            r = forward_pipe.poll(60)
            if r:
                forward_pipe.recv()
            else:
                state = State.ERROR
                break

            # Connect control connection
            control = zmqSvc.socket(zmq.PAIR)
            control.connect(filling_svc.control_connection())

            state = State.RUNNING
        elif (command and command.startswith('stop')
              and state in (State.RUNNING, State.READY)):
            if gaudi:
                gaudi.stop()
            state = State.READY
        elif command and command.startswith('reset'):
            if zmqSvc:
                control.send("TERMINATE")
                if forwarder.is_alive():
                    forwarder.join()

            if gaudi:
                gaudi.finalize()
            state = State.NOT_READY
            break
        elif command is not None:
            print('[ERROR]: RunDB server: bad transition '
                  'from %s to %s' % (state, command))
            state = State.ERROR
            break

        if (command is not None) or (auto and state in (State.NOT_READY,
                                                        State.READY,
                                                        State.RUNNING)):
            # Once we've auto-started, set auto to off to prevent
            # further auto actions, for example after a stop command
            if auto and state == State.RUNNING:
                auto = False
            # Set the status
            com.set_status(state)

        # Handle input from the forwarder
        if (input_socket and input_socket in rep
                and rep[input_socket] == zmq.POLLIN):
            info = input_socket.recv_string()
            # Reset the histograms
            for histo in histos.itervalues():
                histo.Reset()

            for k, v in nodes_info(info).iteritems():
                (host, _, row, node) = k
                gd = v[3]
                if gd != 2:
                    continue
                _, nt = node_type(*k[1:])
                histos[nt].Fill((v[0] - v[1]) / v[0] if v[3] else 0.)

            for histo in histos.itervalues():
                monSvc.publishHistogram("DiskFilling", histo, add=False)

    # Set our status one last time
    com.set_status(state)


if __name__ == '__main__':
    run()
